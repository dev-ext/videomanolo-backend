# Video Manolo´s Backend App
### Stack: Node JS + HAPI + Mongo DB
### Arq Stack: DDD + Clean Architecture
### Author: Brayan Parra.
##
Test for I2B Co

##Docker:
Build App: 
``` 
 $ docker-compose build
```

Run App: 
``` 
$ docker-compose up -d
```

Stop App: 
``` 
$ docker-compose down
```

Logs App: 
``` 
$ docker-compose logs -f
```

Show the app running in URL: http://localhost:3000